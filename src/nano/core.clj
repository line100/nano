(ns nano.core)

(comment
  ;; eval the request with a namespace
  (binding [*ns* (create-ns 'ns-name)]
    (eval (read-string request)))
  )

(defn timer
  [callback &
   {:keys [timeout default]
    :or {timeout 1000
         default :timeout}}]
  (let [fut (future (callback))
        ret (deref fut timeout default)]
    (when (= ret default)
      (future-cancel fut))
    ret))

(defn ns-clean-interns
  "clean up the user variable in ns"
  [ns]
  (map #(ns-unmap ns %)
       (keys (ns-interns ns))))

(defn ns-clean-without
  "clean up the libraries without lst"
  [ns lst]
  (map #(ns-unmap ns %)
       (remove #((set lst) %)
               (-> ns ns-map keys))))
